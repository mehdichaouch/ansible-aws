#!/bin/bash

export AWS_ACCESS_KEY_ID='${your_aws_api_key}'
export AWS_SECRET_ACCESS_KEY='${your_aws_api_secret_key}'
export EC2_REGION='${your_aws_region}'
