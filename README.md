# Ansible AWS

[![build status](https://gitlab.com/klems/ansible-aws/badges/master/build.svg)](https://gitlab.com/klems/ansible-aws/commits/master)

An Easy way to bootstrap an entire AWS infrastructure using Ansible and AWS  

## Install Prerequisites

```
pip install -r requirements.txt
```

## AWS Authentication

Copy the authentication example file in your `$HOME` directory (as a hidden file) :

```
cp -p ~/extras/aws_credentials.sh ~/.aws_credentials.sh
```

Replace the defaults values by yours within the file : `~/.aws_credentials.sh`

Add yourself an alias to source this file :

```
alias auth-aws='source ~/.aws_credentials.sh'
```

Anytime you want to authenticate to interact with your aws account in a terminal, just type `auth-aws`

## Ansible Playbooks

### Virtual Private Clouds

```
ansible-playbook aws_vpc_mgmt.yml -e "env=(dev|staging|prod)"
```

### Security Groups

```
ansible-playbook aws_sg_mgmt.yml -e "env=(dev|staging|prod)"
```

### Subnets

```
ansible-playbook aws_subnet_mgmt.yml -e "env=(dev|staging|prod)"
```

### Internet Gateways

```
ansible-playbook aws_igw_mgmt.yml -e "env=(dev|staging|prod)"
```

### Route Tables

```
ansible-playbook aws_rtbl_mgmt.yml -e "env=(dev|staging|prod)"
```

### Elastic Load Balancers

```
ansible-playbook aws_elb_mgmt.yml -e "env=(dev|staging|prod)"
```

### Provisioning Instances

```
ansible-playbook aws_provisioning_instances.yml -i inventory -e "env=(dev|staging|prod)"
```

### Stopping Instances

```
ansible-playbook aws_stop_instances.yml -i inventory -e "env=(dev|staging|prod)"
```

### Starting Instances

```
ansible-playbook aws_start_instances.yml -i inventory -e "env=(dev|staging|prod)"
```
